import React from "react";

import { Form, Input, Button, Select } from "antd";

const { Option } = Select;

const tagsChildren = [];
const participantsChildren = [];

export default class CreateView extends React.Component {
  render() {
    return (
      <Form>
        <Form.Item label="Titulo">
          <Input placeholder="Ingrese titulo" />
        </Form.Item>
        <Form.Item label="Etiquetas">
          <Select
            mode="tags"
            style={{ width: "100%" }}
            placeholder="Ingrese Tags"
          >
            {tagsChildren}
          </Select>
        </Form.Item>
        <Form.Item label="Curso">
          <Select placeholder="Seleccione Curso">
            <Option value="1">Curso 1</Option>
            <Option value="2">Curso 2</Option>
            <Option value="3">Curso 3</Option>
            <Option value="4">Curso 4</Option>
            <Option value="5">Curso 5</Option>
          </Select>
        </Form.Item>
        <Form.Item label="Integrantes">
          <Select
            mode="tags"
            style={{ width: "100%" }}
            placeholder="Ingrese Correos"
          >
            {participantsChildren}
          </Select>
        </Form.Item>
        <Form.Item>
          <Button type="primary" href="/">
            Crear
          </Button>
        </Form.Item>
      </Form>
    );
  }
}
