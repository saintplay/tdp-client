/** @jsx jsx */
import { css, jsx } from "@emotion/core";
import React from "react";
import axios from "axios";
import classNames from "classnames";

import { Card, Avatar, Col, Row, Typography } from "antd";

const { Title } = Typography;
const { Meta } = Card;

export default class HomeView extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      projects: [],
      internalSelectedIndex: null
    };
  }

  componentDidMount() {
    axios
      .get("http://207.246.73.78:3000/api/Cases")
      .then(response => this.setState({ projects: response.data }));
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.selectedIndex !== prevState.internalSelectedIndex) {
      return { internalSelectedIndex: nextProps.selectedIndex };
    } else return null;
  }

  render() {
    return (
      <div>
        <div>
          <Title>Mis Casos</Title>
        </div>
        <Row type="flex" gutter={16}>
          {this.state.projects.map((project, index) => (
            <Col span={6} key={project.id}>
              <Card
                css={css`
                  &.is-active {
                    border: 4px solid #001529;
                  }
                `}
                className={classNames({
                  "is-active": this.state.internalSelectedIndex === index
                })}
                hoverable
                cover={
                  <img
                    alt=""
                    src={`//loremflickr.com/400/400/dogs?lock=${index}`}
                  />
                }
                style={{ marginTop: "12px" }}
                onClick={() => this.props.handleCardClick(index)}
              >
                <Meta
                  avatar={
                    <Avatar
                      src={`//loremflickr.com/60/60/people?lock=${index}`}
                    />
                  }
                  title={project.title}
                  description={project.description}
                />
              </Card>
            </Col>
          ))}
        </Row>
      </div>
    );
  }
}
