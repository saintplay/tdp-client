import React from "react";

import { Form, Input, Button, Select } from "antd";

const { Option } = Select;

const tagsChildren = [
  <Option key={0}>TF</Option>,
  <Option key={1}>Octubre</Option>
];
const participantsChildren = [
  <Option key={0}>usuario1@gmail.com</Option>,
  <Option key={1}>usuario2@gmail.com</Option>,
  <Option key={2}>usuario3@gmail.com</Option>
];

export default class EditView extends React.Component {
  render() {
    return (
      <Form>
        <Form.Item label="Titulo">
          <Input placeholder="Ingrese titulo" value="Proyecto 4" />
        </Form.Item>
        <Form.Item label="Etiquetas">
          <Select
            mode="tags"
            style={{ width: "100%" }}
            value={["TF", "Octubre"]}
            placeholder="Ingrese Tags"
          >
            {tagsChildren}
          </Select>
        </Form.Item>
        <Form.Item label="Curso">
          <Select defaultValue="3" placeholder="Seleccione Curso">
            <Option value="1">Curso 1</Option>
            <Option value="2">Curso 2</Option>
            <Option value="3">Curso 3</Option>
            <Option value="4">Curso 4</Option>
            <Option value="5">Curso 5</Option>
          </Select>
        </Form.Item>
        <Form.Item label="Integrantes">
          <Select
            mode="tags"
            style={{ width: "100%" }}
            value={[
              "usuario1@gmail.com",
              "usuario2@gmail.com",
              "usuario3@gmail.com"
            ]}
            placeholder="Ingrese Correos"
          >
            {participantsChildren}
          </Select>
        </Form.Item>
        <Form.Item>
          <Button type="primary" href="/">
            Guardar
          </Button>
        </Form.Item>
      </Form>
    );
  }
}
