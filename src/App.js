import React, { Component } from "react";
import "./App.css";
import {
  withRouter,
  BrowserRouter as Router,
  Route,
  Link
} from "react-router-dom";

import { Layout, Menu, Icon } from "antd";
import HomeView from "./views/HomeView";
import SharedView from "./views/SharedView";
import TrashView from "./views/TrashView";
import CreateView from "./views/CreateView";
import EditView from "./views/EditView";

const { Header, Footer, Sider, Content } = Layout;

class AppHeaderMenu extends Component {
  render() {
    if (this.props.location.pathname !== "/") return null;
    return (
      <Menu theme="dark" mode="horizontal" style={{ lineHeight: "64px" }}>
        {this.props.selectedIndex === null && (
          <Menu.Item>
            <a href="/create">
              <Icon type="plus" />
              <span>Nuevo</span>
            </a>
          </Menu.Item>
        )}
        {this.props.selectedIndex !== null && (
          <Menu.Item>
            <a href="/edit">
              <Icon type="edit" />
              <span>Editar</span>
            </a>
          </Menu.Item>
        )}
        {this.props.selectedIndex !== null && (
          <Menu.Item>
            <a href="/trash">
              <Icon type="delete" />
              <span>Eliminar</span>
            </a>
          </Menu.Item>
        )}
      </Menu>
    );
  }
}

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedIndex: null
    };
  }

  handleCardClick(index) {
    if (this.state.selectedIndex === index) {
      this.setState({ selectedIndex: null });
    } else {
      this.setState({ selectedIndex: index });
    }
  }

  render() {
    const RuterAppHeaderMenu = withRouter(AppHeaderMenu);
    return (
      <Router>
        <Layout style={{ minHeight: "100vh" }}>
          <Sider>
            <div style={{ textAlign: "center", padding: "16px 0" }}>
              <div
                style={{
                  display: "inline-block",
                  background: "white",
                  width: "80px",
                  height: "80px",
                  borderRadius: "50%"
                }}
              />
            </div>
            <Menu theme="dark" defaultSelectedKeys={["1"]} mode="inline">
              <Menu.Item key="1">
                <Link to="/">
                  <Icon type="snippets" />
                  <span>Mis Casos</span>
                </Link>
              </Menu.Item>
              <Menu.Item key="2">
                <Link to="/shared">
                  <Icon type="deployment-unit" />
                  <span>Compartidos</span>
                </Link>
              </Menu.Item>
              <Menu.Item key="3">
                <Link to="/trash">
                  <Icon type="delete" />
                  <span>Papelera</span>
                </Link>
              </Menu.Item>
            </Menu>
          </Sider>
          <Layout>
            <Header>
              <RuterAppHeaderMenu selectedIndex={this.state.selectedIndex} />
            </Header>
            <Content style={{ padding: "20px" }}>
              <Route
                exact
                path="/"
                component={() => (
                  <HomeView
                    selectedIndex={this.state.selectedIndex}
                    handleCardClick={this.handleCardClick.bind(this)}
                  />
                )}
              />
              <Route exact path="/shared" component={SharedView} />
              <Route exact path="/create" component={CreateView} />
              <Route exact path="/edit" component={EditView} />
              <Route exact path="/trash" component={TrashView} />
            </Content>
            <Footer style={{ textAlign: "center" }}>
              TDP Asignación ©2019 UPC - Perú
            </Footer>
          </Layout>
        </Layout>
      </Router>
    );
  }
}

export default App;
